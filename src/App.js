import React, { useState } from 'react';
import ContactList from './components/ContactList.js';
import './css/main.css';

function App() {
	const [filter, setFilter] = useState('');
	return (
		<div className="App">
      <header className="app-header">
        <h1>Contacts</h1>
      </header>
			<div className="contact-filter">
				<input type="text" placeholder="Type here to search" onChange={(evt) => setFilter(evt.target.value)} />
			</div>
			<ContactList filter={filter}></ContactList>
		</div>
	);
}

export default App;

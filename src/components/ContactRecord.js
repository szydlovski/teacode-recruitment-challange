import React from 'react';

function EmphasizePhrase({ text, phrase }) {
	const phraseIndex = text.toLowerCase().indexOf(phrase.toLowerCase());
	if (phraseIndex === -1 || phrase.trim() === '') {
		return <span>{text}</span>
	}
	const frontPart = text.slice(0, phraseIndex);
	const middlePart = text.slice(phraseIndex, phraseIndex + phrase.length);
	const backPart = text.slice(phraseIndex + phrase.length, text.length);
	return (
		<span>{frontPart}<strong>{middlePart}</strong>{backPart}</span>
	)
}

export default function ContactRecord({ contact, filter, isSelected, setSelected }) {
	const fullName = `${contact.first_name} ${contact.last_name}`;
	return (
		<li
			className={
				isSelected ? 'contact-record contact-record--selected' : 'contact-record'
			}
			key={contact.id}
			onClick={() => setSelected(contact, !isSelected)}
		>
			<div className="contact-avatar">
				<img
					src={contact.avatar || 'img/default-avatar.png'}
					alt={`${fullName}'s avatar`}
				/>
			</div>
			<div className="contact-name">
				<EmphasizePhrase phrase={filter} text={fullName}></EmphasizePhrase>
			</div>
			<div className="contact-select">
				<input readOnly type="checkbox" checked={isSelected} />
			</div>
		</li>
	);
}

import React, { useState, useEffect, useMemo } from 'react';
import ContactRecord from './ContactRecord.js';

export default function ContactList({ filter }) {
	const [dataLoaded, setDataLoaded] = useState(false);
	const [dataLoadingError, setDataLoadingError] = useState(false);
	const [contactData, setContactData] = useState([]);
	const [selectedContactIds, setSelectedContactIds] = useState([]);

	const filteredContacts = useMemo(
		() =>
			contactData.filter(
				(contact) =>
					filter.trim() === '' ||
					`${contact.first_name} ${contact.last_name}`
						.toLowerCase()
						.includes(filter.trim().toLowerCase())
			),
		[contactData, filter]
	);

	const setSelected = (contact, isSelected) => {
		const index = selectedContactIds.indexOf(contact.id);
		let newSelection;
		if (!isSelected && index !== -1) {
			newSelection = [
				...selectedContactIds.slice(0, index),
				...selectedContactIds.slice(index + 1, selectedContactIds.length),
			];
		} else if (isSelected && index === -1) {
			newSelection = [...selectedContactIds, contact.id];
		}
		if (newSelection !== undefined) {
			setSelectedContactIds(newSelection);
			console.log(newSelection);
		}
	};

	useEffect(() => {
		(async () => {
			try {
				const response = await fetch(
					'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json'
				);
				const rawUserData = await response.json();
				const sortedUserData = rawUserData.sort((contact1, contact2) =>
					contact1.last_name.localeCompare(contact2.last_name)
				);
				setContactData(sortedUserData);
				setDataLoaded(true);
			} catch (error) {
				setDataLoadingError(true);
			}
		})();
	}, []);

	if (dataLoadingError) {
		return (
			<div className="contact-list-message">An error occured while loading contacts.</div>
		);
	} else if (!dataLoaded) {
		return <div className="contact-list-message">Loading contacts...</div>;
	} else if (filteredContacts.length === 0) {
		return <div className="contact-list-message">No contacts match the search.</div>;
	} else {
		return (
			<div>
				<ul className="contact-list">
					{filteredContacts.map((user) => (
						<ContactRecord
							contact={user}
							key={user.id}
							filter={filter}
							isSelected={selectedContactIds.includes(user.id)}
							setSelected={setSelected}
						></ContactRecord>
					))}
				</ul>
			</div>
		);
	}
}
